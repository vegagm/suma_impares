# **********************************************************************
#             IGSAM                    Informatica I
# **********************************************************************
#                          SUMA_IMPARES
# **********************************************************************
#                     Vega González Martín
# **********************************************************************

#llamamos numero1 y numero2 como n1 y n2

try:
    seguir = True
    while seguir:
        n1 = int(input("Dame un número entero no negativo"))
        if n1 <=0:
            print("El número tiene que ser entero no negativo")
        else:
            seguir = False

except ValueError:
    print("Dame un número entero no negativo: ")

try:
    seguir = True
    while seguir:
        n2 = int(input("Dame un número entero no negativo"))
        if n2 <= 0:
            print("El número tiene que ser entero no negativo")
        else:
            seguir = False

except ValueError:
    print("Dame un número entero no negativo: ")

suma = 0
#Si n1 es mayor que n2 los intercambio.

if n1 <=n2:
    n = n1
    n1 = n2
    n2 = n

#Recorremos los números entre n1 y n2
for i in range(n1,n2+1,1):
#Sumo solo los impares porque cualquier número impar dividido entre dos queda su resto 1 y si es par su resto queda 0.
#Así que preguntamos al programa si es impar y si lo es lo suma.
    if i%2 !=0:
        suma = suma + i
#Imprimimos el resultado con la cadena f".
print(f"la suma de los números impares entre {n1} y {n2} es {suma}")






